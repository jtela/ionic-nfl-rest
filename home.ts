import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  teams: any;
  errorMessage: string;

  constructor(public navCtrl: NavController, public rest: RestProvider) {

  }

  ionViewDidLoad() {
    this.getTeams();
  }

  getTeams() {
    this.rest.getTeams()
       .subscribe(
         teams => this.teams = teams,
         error =>  this.errorMessage = <any>error);
  }

}